from rest_framework import serializers
from api.models import Encounter, Soap, UserData
from .user import UserSerializer

class EncounterSerializer(serializers.ModelSerializer):

    provider_id = UserSerializer(read_only=True)

    class Meta:
        model = Encounter
        fields = (
                'id',
                'date',
                'onset_date',
                'reason',
                'facility',
                'encounter',
                'sensitivity',
                'provider_id'
                )

class SOAPSerializer(serializers.ModelSerializer):

    class Meta:
        model = Soap
        fields = (
                'id',
                'date',
                'subjective',
                'objective',
                'assessment',
                'plan',
                )