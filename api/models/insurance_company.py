from django.db import models
from api.utilities import utils

class InsuranceCompanies(models.Model):

    #id = models.IntegerField(primary_key=True, default=utils.get_max_ins_id(), editable=False)
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    attn = models.CharField(max_length=255, blank=True)
    cms_id = models.CharField(max_length=15, blank=True)
    ins_type_code = models.IntegerField(
        blank=True,
        default=2,
        choices = utils.PAYER_TYPES)
    x12_receiver_id = models.CharField(max_length=25,blank=True)
    x12_default_partner_id = models.IntegerField(blank=True)
    alt_cms_id = models.CharField(max_length=15, blank=True)
    inactive = models.IntegerField(blank=True, default=0)

    def __str__(self):
        return str(self.id) + ' - ' + self.name

    class Meta:
        managed = False
        db_table = 'insurance_companies'

class InsuranceAddress(models.Model):
    id = models.AutoField(primary_key=True)
    #id = models.AutoField(primary_key=True, default=utils.get_max_ins_addr_id(), editable=False)
    line1 = models.CharField(max_length=255, blank=True)
    line2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = models.CharField(max_length=35, blank=True)
    zip = models.CharField(max_length=10, blank=True)
    plus_four = models.CharField(max_length=4, blank=True)
    country = models.CharField(max_length=255, blank=True, default='USA')
    #foreign_id = models.ForeignKey(
    #    InsuranceCompanies,
    #    on_delete=models.CASCADE,
    #    related_name="address",
    #    db_column='foreign_id'
    #)
    foreign_id = models.ForeignKey(
        InsuranceCompanies,
        related_name="insurance_address",
        on_delete=models.CASCADE,
        db_column='foreign_id',
    )

    class Meta:
        managed = False
        db_table = 'addresses'
