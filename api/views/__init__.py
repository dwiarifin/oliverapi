from api.views.patient import PatientList, PatientDetail
from api.views.insurance import InsuranceList, InsuranceDetail
from api.views.insurance_company import InsuranceCompanyList, InsuranceCompanyDetail, InsuranceAddressList, InsuranceAddressDetail
from api.views.encounter import EncounterList
from api.views.provider import ProviderList, ProviderDetail
# # Api Root Must Last import
# from api.views.api_root import ApiRoot
