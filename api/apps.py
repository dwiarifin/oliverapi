from django.apps import AppConfig

app_label = 'api'
class ApiConfig(AppConfig):
    name = 'api'
